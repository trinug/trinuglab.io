const C3 = self.C3;
self.C3_GetObjectRefTable = function () {
	return [
		C3.Plugins.Sprite,
		C3.Behaviors.EightDir,
		C3.Behaviors.bound,
		C3.Behaviors.Bullet,
		C3.Behaviors.Sin,
		C3.Behaviors.Physics,
		C3.Behaviors.Fade,
		C3.Plugins.Text,
		C3.Plugins.TiledBg,
		C3.Plugins.Mouse,
		C3.Plugins.System.Cnds.Every,
		C3.Plugins.Sprite.Acts.Spawn,
		C3.Plugins.System.Acts.CreateObject,
		C3.Plugins.System.Exps.random,
		C3.Plugins.Sprite.Cnds.OnCollision,
		C3.Plugins.Sprite.Acts.Destroy,
		C3.Plugins.System.Cnds.PickRandom,
		C3.Plugins.Sprite.Acts.SubInstanceVar,
		C3.Plugins.System.Acts.AddVar,
		C3.Plugins.Sprite.Cnds.CompareInstanceVar,
		C3.Plugins.System.Acts.Wait,
		C3.Plugins.System.Acts.GoToLayout,
		C3.Plugins.Mouse.Cnds.OnObjectClicked
	];
};
self.C3_JsPropNameTable = [
	{heart: 0},
	{"8Direction": 0},
	{BoundToLayout: 0},
	{Player: 0},
	{Bullet: 0},
	{Heart: 0},
	{Sine: 0},
	{Enemy: 0},
	{Enemy2: 0},
	{Physics: 0},
	{Rocket: 0},
	{Fade: 0},
	{Kabooms: 0},
	{enemy3: 0},
	{enemyBullet: 0},
	{Text: 0},
	{enemy4: 0},
	{backgrond: 0},
	{Logo: 0},
	{PlayButton: 0},
	{Mouse: 0},
	{Point: 0}
];

self.InstanceType = {
	Player: class extends self.ISpriteInstance {},
	Bullet: class extends self.ISpriteInstance {},
	Enemy: class extends self.ISpriteInstance {},
	Enemy2: class extends self.ISpriteInstance {},
	Rocket: class extends self.ISpriteInstance {},
	Kabooms: class extends self.ISpriteInstance {},
	enemy3: class extends self.ISpriteInstance {},
	enemyBullet: class extends self.ISpriteInstance {},
	Text: class extends self.ITextInstance {},
	enemy4: class extends self.ISpriteInstance {},
	backgrond: class extends self.ITiledBackgroundInstance {},
	Logo: class extends self.ISpriteInstance {},
	PlayButton: class extends self.ISpriteInstance {},
	Mouse: class extends self.IInstance {}
}